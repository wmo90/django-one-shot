from todos.models import TodoList, TodoItem
from django.forms import ModelForm
from django import forms


class TodoForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TaskForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
