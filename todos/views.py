from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TaskForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {"list_detail": list_detail}
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list_detail = form.save()
            list_detail.save()
            return redirect("todo_list_detail", id=list_detail.id)
    else:
        form = TodoForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=update)
    context = {"update_form": form, "update_object": update}
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    update = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=update.list.id)
    else:
        form = TaskForm(instance=update)
    context = {"update_object": update, "update_form": form}
    return render(request, "todos/update_item.html", context)
